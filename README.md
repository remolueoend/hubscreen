# Hubscreen

## Stats
- hostname: `raspberrypi` (sorry, forgot to change it...)
- IP: `10.2.3.57`
- default user: `pi`
- root/pi password: see `pass show colab/hub-screen/pi`

## Setup Rasperry Pi

### restore from backup
If a backup image is available (e.g. `./backup.img` in root of this repository) and restoring it is an option, then follow the [tools-README](./packages/tools/README.md) to restore the backup.


### setup from scratch (headless)
See [the official docs](https://www.raspberrypi.org/documentation/installation/installing-images/) for more details. What follows are the most important steps:

Download and flash [Raspbian Lite](https://downloads.raspberrypi.org/raspbian_lite_latest) onto the SD-card. Use [etcher](https://www.balena.io/etcher/) or similar tools to do so.

To connect remotely over the network requires following changes to the SD-card:
* Put an empty file called `ssh` into the root of the *boot partition* to enable remote [ssh access](https://www.raspberrypi.org/documentation/remote-access/ssh/README.md).
* Uncomment following lines in `/boot_partition/config.txt`:
```
hdmi_force_hotplug=1
hdmi_group=2
hdmi_mode=82
```
* Switch off on-board sound: `dtparam=audio=off` in `/boot/config.txt`

Depending on your location and setup, there are following ways to connect to the raspberry pi initially:
#### You're at ImpactHub
Just connect the raspberry pi (MAC: `b8:27:eb:41:40:0c`) with the impact hub network via eth0. It will get the correct IP listed at the top automatically.
If you're using a new device, register it with the DHCP.

#### Connect via Wifi
Adjust the WiFi network infos in `./config_templates/wpa_supplicant.conf` and copy the whole file to `/boot_partition/boot/`.
Follow one of the next two topics to find the IP of your connected raspberry pi:

##### Configure a static IP
Add the following static IP infos to the bottom of `./config_templates/dhcpcd` and copy the whole file to `/OS_partition/etc/`:
```
    interface eth0
    static ip_address=10.2.3.57/16
    static routers=10.2.0.1
    static domain_name_servers=10.2.0.1
```
see [static ip](https://www.raspberrypi.org/documentation/configuration/tcpip/README.md) for more details.

##### Connect wie mDNS host name resolution
Write the desired hostname into `OS_partition/etc/hostname` and use an mDNS service (such as Avahi) to discover the IP of the device as soon as connected to the network.


### Setup system
* After first log-in, change password (re-use existing @ `pass show colab/hub-screen/pi`)

### Setup Server and Client

The setup process is split into two parts: Setting up the flaschentaschen server (always required) and optionally setting up the python client provided in this repo under `packages/client`.

First thing to do is to setup basic software dependencies:

```sh
sudo apt update
sudo apt install git
```

Because both the server and the client are part of this mono-repo, the next step is **recursively** cloning this repo under `~/hubscreen` on the RaspberryPi:

```sh
git clone --recursive-submodules https://git.panter.ch/open-source/hubscreen.git ~/hubscreen
```

Then follow the instructions in the [Server-README](./packages/server/README.md) and [Client-README](./packages/client/README.md)
