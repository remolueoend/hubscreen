# Hubscreen Client

## Development

### Setup

On OSX, you might have to increase the maximum allowed UDP message size using the following command: `sudo sysctl -w net.inet.udp.maxdgram=65535`

This setup makes use of ImageMagick to generate images sent to the FlaschenTaschen server. before installing pip dependencies, following system dependencies have to be installed:

- ImageMagick for pgmagick, see: https://pypi.org/project/pgmagick/

This python project uses `pipenv` to manage all python dependencies, which allows locking down dependencies and their versions using a `Pipfile.lock` file, and installing project dependencies directly in a virtual environment to avoid conflicts with other pip packages installed on the local system:

- install pipenv: https://pipenv.readthedocs.io
- activate environment: `pipenv shell` (working directory has to be `./packages/client`)
- install all project dependencies: `pipenv install`

## Setup Client on the Raspberry Pi

The following documentation assumes you followed the [initial setup steps](../../README.md) first.

The code of the python client is located at `./packages/client`. All following commands are executed in this directory.

First install all required dependencies by executing `./setup/install_dependencies.sh`. This might take a while, better grab a coffee. If it seems to hang, it probably doesn't. Good measure is to use `htop` to check if any gcc/python process is still heavily working in the background.

To verify the setup, use the following command to start the client manually:

```sh
python src/main.py
```

If everything works, install the systemd service for the Hubscreen client using following commands:

```sh
sudo ./install_service.sh
```

To verify the correct setup of the hubscreen_client service, use `systemctl` and `journalctl` commands, such as`journalctl -u hubscreen_client.service`or`systemctl status hubscreen_client.service`.
