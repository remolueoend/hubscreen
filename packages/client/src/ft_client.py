# -*- mode: python; c-basic-offset: 2; indent-tabs-mode: nil; -*-
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation version 2.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://gnu.org/licenses/gpl-2.0.txt>

from socket import socket, AF_INET, SOCK_DGRAM
from pgmagick import Blob
from logging import getLogger

logger = getLogger(__name__)


class FlaschenClient(object):
    """
    A Framebuffer display interface that sends a frame via UDP.
    """

    def __init__(self, host, port, layer=5, transparent=False):
        """
        Args:
          host: The flaschen taschen server hostname or ip address.
          port: The flaschen taschen server port number.
          layer: The layer of the flaschen taschen display to write to.
          transparent: If true, black(0, 0, 0) will be transparent and show the layer below.
        """
        logger.info("connecting to %s:%d...", host, port)
        self.layer = layer
        self.transparent = transparent
        self._sock = socket(AF_INET, SOCK_DGRAM)
        self._sock.connect((host, port))

        # self._data = bytearray(width * height * 3 + len(header) + len(footer))

    def send(self, image):
        """
        Send the updated pixels to the display.
        """

        image.magick("ppm")
        blob = Blob()
        # PPM with color depth 8:
        image.write(blob, "PPM", 8)
        self._sock.send(blob.data)

    def close(self):
        self._sock.close()
