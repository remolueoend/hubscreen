from canvas.Drawable import Drawable
from pgmagick import DrawableText, Color


class Text(Drawable):
    def_props = dict(font="./fonts/5x7.bdf",
                     color="white", text="", bold=False)

    def font(self, font):
        return self.update("font", font)

    def color(self, color):
        return self.update("color", color)

    def text(self, text):
        return self.update("text", text)

    def render(self):
        return []

    def draw(self, image):
        (x, y) = self.get_pos()
        image.font(self.prop("font"))
        image.fillColor(self.prop("color"))
        image.draw(DrawableText(x, y, self.prop("text")))


TEXT = Text()
