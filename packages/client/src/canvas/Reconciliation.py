"""
type COMPONENT
type TREE_NODE(COMPONENT, [COMPONENT])
"""


def get_node_children(component):
    """
    Accepts a COMPONENT. Returns its children by rendering the component.
    """
    children = component.render()
    return children if type(children) == list else [children]


def generate_tree(component, updater):
    """
    Accepts a COMPONENT. Renders it recusively using `render()` and returns its equivalent
    node tree as TREE_NODE.
    on the given instance, fires events and attaches all necessary side effect handlers.
    """
    child_nodes = list(
        map(lambda child: generate_tree(child, updater), get_node_children(component)))

    component.updater = updater
    component.setup()

    return (component, child_nodes)


def teardown_tree(tree_node):
    (component, children) = tree_node
    for child in children:
        teardown_tree(child)
    component.teardown()


def reconcile_component(curr_node, new, updater):
    """
    curr_node: TREE_NODE
    new      : Component
    Return either a new TREE_NODE or the given current one, but with updated props
    """
    curr = curr_node[0]
    new_type, new_props = type(new), new._props
    curr_type, curr_props = type(curr), curr._props

    if new_type != curr_type:
        teardown_tree(curr_node)
        return generate_tree(new, updater)

    if curr_props != new_props:
        curr._props = new._props
        curr.on_props_updated(curr_props, new_props)

    return curr_node


def reconcile_recursively(tree_node, updater):
    (current_component, current_children) = tree_node
    new_children = get_node_children(current_component)

    # todo: only re-create children which actually changed
    if len(new_children) != len(current_children):
        for child in current_children:
            teardown_tree(child)
        return (current_component, list(map(lambda child: generate_tree(child, updater), new_children)))

    updated_children = []
    for i in range(0, len(current_children)):
        updated_children.append(reconcile_component(
            current_children[i], new_children[i], updater))

    return (current_component, list(map(lambda child: reconcile_recursively(child, updater), updated_children)))


class Reconciliation:
    def __init__(self, root_component, updater):
        self.current_tree = generate_tree(root_component, updater)
        self.updater = updater

    def reconcile(self):
        self.current_tree = reconcile_recursively(
            self.current_tree, self.updater)
