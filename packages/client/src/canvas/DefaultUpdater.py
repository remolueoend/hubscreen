from queue import Queue
from threading import Thread
from time import sleep
from logger import getLogger

logger = getLogger(__name__)


def noop_update():
    pass


def state_update(component, partial_state):
    def updater():
        component.state = {**component.state, **partial_state}
    return updater


class DefaultUpdater:
    def __init__(self, image):
        self.image = image
        self.update_queue = Queue(0)
        self.udpate_thread = Thread(target=self.process)

    def start(self):
        self.udpate_thread.start()
        return self.udpate_thread

    def process(self):
        while True:
            sleep(1/60)
            pending_updates = [self.update_queue.get(True)]
            while self.update_queue.qsize() != 0:
                pending_updates.append(self.update_queue.get(True))
            for update in pending_updates:
                update()
            self.image.render()

    def update_state(self, component, partial_state):
        # logger.debug("received state update")
        self.update_queue.put(state_update(component, partial_state))

    def force_update(self):
        self.update_queue.put(noop_update)
