from canvas import Component


class Group(Component):
    def render(self):
        (x, y) = self.get_pos()
        # return children with updated positions, relative to own position:
        return list(map(lambda child: child.pos(x + child.prop("x"), y + child.prop("y")), self.prop("children")))


GROUP = Group()
