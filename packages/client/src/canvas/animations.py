from canvas import Component
from datetime import datetime
from threading import Thread, Event
from time import sleep


class BaseAnimation(Component):
    def_props = dict(flag=None)

    def _get_next_progress(self, current_progress):
        raise "Not implemented"

    def _get_next_value(self, next_progress):
        raise "Not implemented"

    def _wait_for(self):
        raise "Not implemented"

    def _should_finish(self, next_progress):
        raise "Not implemented"

    def _final_value(self):
        raise "Not implemented"

    def _initial_value(self):
        raise "Not implemented"

    def __init__(self, props):
        super().__init__(props)
        self.update_thread = None
        self.state = dict(prop_value=self._initial_value())

    def start(self):
        if self.update_thread is None or not self.update_thread.is_alive():
            self.last_update_ts = datetime.now()
            self.current_progress = 0
            self.finished = False
            self._stop_animation = Event()
            self.update_thread = Thread(target=self.progress)
            self.update_thread.start()

    def stop(self):
        if self.update_thread.is_alive():
            self._stop_animation.set()
            self.update_thread.join()

    def setup(self):
        self.start()

    def teardown(self):
        self.stop()

    def progress(self):
        while not self._stop_animation.isSet() and not self.finished:
            sleep(self._wait_for())
            next_progress = self._get_next_progress(self.current_progress)
            finished = self._should_finish(next_progress)
            self.set_state(
                {"prop_value": self._final_value() if finished else self._get_next_value(next_progress)})
            self.last_update_ts = datetime.now()
            self.finished = finished
            self.current_progress = next_progress

        if self.finished and self.has_prop("on_finish"):
            self.prop("on_finish")()

    def render(self):
        children = self.children()
        if len(children) != 1:
            raise "Invalid number of children"
        return children[0].update(self.prop("prop"), self.state["prop_value"])

    def on_props_updated(self, old_props, new_props):
        if old_props["flag"] != new_props["flag"]:
            self.stop()
            self.start()


class LinearAnimation(BaseAnimation):

    def __init__(self, props):
        return super().__init__(props)
        self.state = dict(**self.state, progress=self.prop("initial"))

    def _get_step_size_per_ms(self):
        (start, end, duration) = self.prop("initial", "final", "duration")
        return (end - start) / duration

    def _wait_for(self):
        return 1 / 60

    def _should_finish(self, next_progress):
        (start, end) = self.prop("initial", "final")
        return False if min(start, end) <= next_progress <= max(
            start, end) else True

    def _final_value(self):
        return self.prop("final")

    def _initial_value(self):
        return self.prop("initial")

    def _get_next_progress(self, current_progress):
        now = datetime.now()
        passed_ms = (now - self.last_update_ts).microseconds / 1000
        return current_progress + self._get_step_size_per_ms() * passed_ms

    def _get_next_value(self, next_progress):
        return next_progress


class StepAnimation(BaseAnimation):
    def _wait_for(self):
        return self.prop("duration") / 1000

    def _should_finish(self, next):
        return next >= self.prop("n")

    def _final_value(self):
        (initial, size, n) = self.prop("initial", "size", "n")
        return initial + n * size

    def _initial_value(self):
        return self.prop("initial")

    def _get_next_progress(self, current_progress):
        return current_progress + 1

    def _get_next_value(self, current_progress):
        (initial, size) = self.prop("initial", "size")
        return initial + current_progress * size
