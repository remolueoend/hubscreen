from canvas import Component


class Drawable(Component):
    def draw(self, image, x, y):
        raise NotImplementedError()
