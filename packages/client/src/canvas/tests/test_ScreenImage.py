from canvas import Component
from canvas.ScreenImage import reconcile_nodes
from tests import TestCaseWithCustomAssertions
from canvas.Component import NoopUpdater
from unittest import SkipTest

def_updater = NoopUpdater()


class TestReconcileNodes(TestCaseWithCustomAssertions):
    def test_old_node_none(self):
        old_node = None
        new_node = (Component, {"foo": 1}, [])

        self.assert_component_equal(reconcile_nodes(
            old_node, new_node, def_updater), Component, {"foo": 1})

    def test_different_type(self):
        class DiffType(Component):
            pass

        old_node = Component()
        new_node = (DiffType, {"foo": 1}, [])

        self.assert_component_equal(reconcile_nodes(
            old_node, new_node, def_updater), DiffType, {"foo": 1})

    def test_updated_props(self):
        old_node = Component(dict(foo=1, baz=2))
        new_node = (Component, dict(foo=2, bang=3), [])

        self.assertEqual(reconcile_nodes(
            old_node, new_node, def_updater), old_node)
        self.assertDictEqual(old_node._props, dict(
            foo=2, bang=3, children=[], x=0, y=0))

    def test_new_number_of_children(self):
        old_node = Component().append([Component(), Component()])
        new_node = (Component, {}, [(Component, {}, [])])

        updated_node = reconcile_nodes(old_node, new_node, def_updater)
        updated_children = updated_node.prop("children")

        self.assertEqual(len(updated_children), 1)
        self.assert_component_equal(updated_children[0], Component, {})

    def test_same_number_of_children_with_same_types(self):
        class DiffType(Component):
            pass
        old_node = Component().append(
            [DiffType({"foo": 1, "baz": 2}), Component({"bang": 3})])
        new_node = (Component, {}, [
            (DiffType, {"foo": 2}, []), (Component, {"bang": 4}, [])
        ])

        updated_children = reconcile_nodes(
            old_node, new_node, def_updater).prop("children")
        self.assertEqual(updated_children,
                         old_node.prop("children"))
        self.assertDictEqual(updated_children[0]._props, dict(
            children=[], x=0, y=0, foo=2))
        self.assertDictEqual(updated_children[1]._props, dict(
            children=[], x=0, y=0, bang=4))
