from logging import getLogger


def get_def_props(type):
    if type == Component:
        return Component.def_props

    if not issubclass(type, Component):
        raise "invalid type, has to be derivded from Component"

    # todo: support multi-inheritance?
    base_type = type.__bases__[0]
    def_props = type.def_props if hasattr(type, "def_props") else {}

    return {**get_def_props(base_type), **def_props}


class NoopUpdater:
    def __init(self):
        self.logger = getLogger(__name__ + ".NoopUpdater")

    def update_state(self, component, partial_state):
        self.logger.warn(
            "Looks like the renderer did not set any updater for the current component: {}", component)

    def force_update(self):
        self.logger.warn(
            "Looks like the renderer did not set any updater for the current component")


class Component(object):
    def_props = dict(children=[], x=0, y=0)

    def __init__(self, props={}):
        self._props = {**get_def_props(type(self)), **props}
        self.updater = NoopUpdater()
        self.state = {}

    def prop(self, *prop_names):
        props_count = len(prop_names)
        if props_count == 0:
            raise "Invalid number of arguments."
        if props_count == 1:
            return self._props[prop_names[0]]
        else:
            return tuple(map(lambda p: self._props[p], prop_names))

    def has_prop(self, name):
        return name in self._props

    def setup(self):
        pass

    def teardown(self):
        pass

    def update(self, prop, value):
        Type = type(self)
        return Type({**self._props, prop: value})

    def updates(self, updates):
        Type = type(self)
        return Type({**self._props, **updates})

    def pos(self, x, y):
        return self.updates({"x": x, "y": y})

    def children(self):
        return self.prop("children")

    def append(self, components):
        return self.update('children', self.prop("children") + (components if type(components) == list else [components]))

    def get_pos(self):
        return (self.prop("x"), self.prop("y"))

    def render(self):
        raise NotImplementedError()

    def set_state(self, partial_state):
        self.updater.update_state(self, partial_state)

    def on_props_updated(self, old_props, new_props):
        pass
