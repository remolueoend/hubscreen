from pgmagick import DrawableLine
from canvas.Drawable import Drawable


class Line(Drawable):
    def_props = dict(width=0, height=0, color="white", thickness=1)

    def width(self, width):
        return self.update("width", width)

    def height(self, height):
        return self.update("height", height)

    def render(self):
        return []

    def draw(self, image):
        (x, y) = self.get_pos()
        image.draw(DrawableLine(
            x, y, x + self.prop("width"), y + self.prop("height")))


LINE = Line()
