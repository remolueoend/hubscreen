from pgmagick import Image, Geometry, Color
from time import sleep
from canvas import Drawable
from canvas.DefaultUpdater import DefaultUpdater
from canvas.Reconciliation import Reconciliation


class ScreenImage(Image):
    def __init__(self, root_component, on_update):
        super().__init__(Geometry(320, 32), Color("black"))
        self.antiAlias(False)
        self.strokeAntiAlias(False)
        self.on_update = on_update
        self.updater = DefaultUpdater(self)
        self.reconciliation = Reconciliation(root_component, self.updater)

    def render_and_wait(self):
        self.updater.force_update()
        self.updater.start().join()

    def render(self):
        self.reconciliation.reconcile()
        self.erase()
        self.draw_tree_node(self.reconciliation.current_tree)
        self.on_update(self)

    def draw_tree_node(self, tree_node):
        """
        Draws the given component
        """
        (component, children) = tree_node
        component_type = type(component)

        if(issubclass(component_type, Drawable)):
            component.draw(self)

        for child in children:
            self.draw_tree_node(child)
