class Memoize:
    def __init__(self, fn):
        self.fn = fn
        self.last_calculation = None

    def _is_stale(self, args, kwargs):
        (cached_args, cached_kwargs, _) = self.last_calculation
        return cached_args != args or cached_kwargs != kwargs

    def __call__(self, *args, **kwargs):
        if(self.last_calculation is None or self._is_stale(args, kwargs)):
            self.last_calculation = (args, kwargs, self.fn(*args, **kwargs))

        return self.last_calculation[2]
