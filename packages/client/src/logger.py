from logging import getLogger, DEBUG, INFO, StreamHandler, Formatter, BASIC_FORMAT
from pythonjsonlogger import jsonlogger
from consts import DEV_ENV

supported_keys = [
    'asctime',
    'created',
    'filename',
    'funcName',
    'levelname',
    'levelno',
    'lineno',
    'module',
    'msecs',
    'message',
    'name',
    'pathname',
    'process',
    'processName',
    'relativeCreated',
    'thread',
    'threadName'
]


def log_format(x): return ['%({0:s})'.format(i) for i in x]


def setup_root_logger(app_env):

    logLevel = DEBUG if app_env == DEV_ENV else INFO

    rootLogFormatter = Formatter(
        "%(asctime)s " + BASIC_FORMAT) if app_env == DEV_ENV else jsonlogger.JsonFormatter(' '.join(log_format(supported_keys)))

    rootLogHandler = StreamHandler()
    rootLogHandler.setLevel(logLevel)
    rootLogHandler.setFormatter(rootLogFormatter)

    rootLogger = getLogger()
    rootLogger.setLevel(logLevel)
    rootLogger.addHandler(rootLogHandler)
