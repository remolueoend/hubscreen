from canvas import Component, GROUP, TEXT
from canvas.utils import map_children

from components.shared import whiteText
from lib import Memoize
import pytz

def format_time(naive_time):
    utc_time = naive_time.replace(tzinfo=pytz.utc)
    local_time = utc_time.astimezone(pytz.timezone('Europe/Zurich'))
    return local_time.strftime("%H:%M")


def formatRoomAndName(booking):
    return "{}: {}".format( booking[0], booking[1])

def formatTimeRange(booking):
    return "{} - {}".format(format_time(booking[2]), format_time(booking[3]))

@Memoize
def get_booking_texts(bookings):

    return [whiteText.text("{:<39.39}  {}".format(formatRoomAndName(booking), formatTimeRange(booking)))
            for booking in bookings if booking is not None]


class BookingsList(Component):
    def render(self):
        (x, y) = self.get_pos()
        group = GROUP.updates(self._props).pos(x, y + 7)
        booking_texts = get_booking_texts(self.prop("bookings"))

        return map_children(
            lambda booking_text, i: booking_text.pos(0, i * 10),
            booking_texts,
            group
        )
