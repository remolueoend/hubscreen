from canvas import Component, GROUP
from components.shared import bigText, smallText, redLine


class ImpactHubLogo(Component):
    def render(self):
        (x, y) = self.get_pos()

        return GROUP.pos(x, y).append([
            smallText.text("Impact").pos(0, 9),
            bigText.text("HUB").pos(2, 20),
            smallText.text("Zürich").pos(0, 28),
            redLine.pos(34, 0)
        ])
