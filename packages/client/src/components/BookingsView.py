from canvas import Component, GROUP, TEXT
from canvas.animations import LinearAnimation, StepAnimation
from components import ImpactHubLogo, BookingsList
from logging import getLogger
from data import fetch_bookings
from data import get_next_booking
from tests.mock_data import bookings
from datetime import datetime

logger = getLogger(__name__)


class BookingsView(Component):
    def __init__(self, props={}):
        super().__init__(props)
        self.state = {**self.state, "bookings": None}

    def setup(self):
        self.fetch_bookings_data()

    def render(self):
        bookings = self.state["bookings"]
        if bookings is None:
            return TEXT.text("Loading...")

        return GROUP.append([
            ImpactHubLogo().pos(3, 0),
            StepAnimation(dict(
                prop="y",
                n=len(bookings),
                initial=2,
                size=-10,
                duration=4000,
                on_finish=self.fetch_bookings_data,
                flag=self.last_updated
            )).append(
                GROUP.pos(45, 2).append([
                    BookingsList({"bookings": bookings}).pos(0, 0),
                    # render the list twice after each other
                    # to simulate a never-ending loop:
                    BookingsList({"bookings": bookings}).pos(
                        0, 10 * len(bookings))
                ])
            )
        ])

    def fetch_bookings_data(self):
        bookings_data = fetch_bookings()
        # bookings_data = bookings
        self.last_updated = datetime.now().microsecond
        roomsWithBookings = bookings_data["data"]["hub"]["rooms"]
        bookingsUncleaned = [get_next_booking(room) for room in roomsWithBookings]
        bookings = [booking for booking in bookingsUncleaned if booking]
        self.set_state({"bookings": bookings})
