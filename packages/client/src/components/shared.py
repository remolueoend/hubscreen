from canvas import TEXT, LINE
from pgmagick import Color

redText = TEXT.color(Color("red"))
whiteText = TEXT.color(Color("white"))
smallText = redText.font("./fonts/5x7.bdf")
bigText = redText.font("./fonts/7x9.bdf")
redLine = LINE.width(0).height(32)
