from graphqlclient import GraphQLClient
import json
from datetime import datetime

from time import sleep

client = GraphQLClient('https://api.hub.panter.cloud/graphql')


def fetch_bookings():
    result = client.execute('''
    {
        hub(id: "zürich-colab") {
            rooms {
                name
                _id
                currentBooking {
                    title
                    time {
                        from
                        to
                    }
                }
                nextBookingToday {
                    title
                    time {
                        from
                        to
                    }
                }
            }
        }
    }
    ''')

    return json.loads(result)


def parse_time(str):
    return datetime.strptime(str, "%Y-%m-%dT%H:%M:%S.%fZ")


def get_next_booking(room):
    currentBooking = room["currentBooking"]
    nextBookingToday = room["nextBookingToday"]
    next_booking = currentBooking if currentBooking else nextBookingToday
    if next_booking is not None:
        from_time = parse_time(
            next_booking["time"]["from"])
        to_time = parse_time(
            next_booking["time"]["to"])
        booker = next_booking["title"]
        room_name = room["name"]
        return (room_name, booker, from_time, to_time)
    else:
        return None
