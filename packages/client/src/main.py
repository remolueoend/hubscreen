from ft_client import FlaschenClient
from canvas import ScreenImage
from components import BookingsView
from envparse import env
from logging import getLogger, DEBUG, INFO, StreamHandler, Formatter, BASIC_FORMAT
from pythonjsonlogger import jsonlogger
from consts import PROD_ENV
from logger import setup_root_logger
from data import fetch_bookings
from tests.mock_data import bookings

app_env = env.str("APP_ENV", default=PROD_ENV)

setup_root_logger(app_env)

server_host = env.str("FT_SERVER_HOST", default="localhost")
server_port = env.int("FT_SERVER_PORT", default=1337)

client = FlaschenClient(server_host, server_port)

im = ScreenImage(BookingsView(), lambda image: client.send(image))
im.render_and_wait()

client.close()
