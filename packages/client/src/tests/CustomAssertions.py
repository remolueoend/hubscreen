from unittest import TestCase


class TestCaseWithCustomAssertions(TestCase):
    def assert_component_equal(self, actual, expected_type, expected_props):
        non_def_props = actual._props.copy()
        del non_def_props["children"]
        del non_def_props["x"]
        del non_def_props["y"]

        self.assertEqual(type(actual), expected_type)
        self.assertDictEqual(non_def_props, expected_props)
