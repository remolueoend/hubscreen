bookings = {
    "data": {
        "hub": {
            "rooms": [
                {"name": "C1", "_id": "C1", "bookings": [{
                    "bookerName": "B. Seeliger",
                    "time": {
                        "from": "2019-06-21T10:30:00.000Z",
                        "to": "2019-06-21T11:30:00.000Z"
                    }
                }]},
                {"name": "C3", "_id": "C3", "bookings": [{
                    "bookerName": "R. Zumsteg",
                    "time": {
                        "from": "2019-06-21T09:30:00.000Z",
                        "to": "2019-06-21T10:00:00.000Z"
                    }
                }]},
                {"name": "Bogen D", "_id": "Bogen_D", "bookings": [{
                    "bookerName": "D. Baldinis",
                    "time": {
                        "from": "2019-06-21T14:30:00.000Z",
                        "to": "2019-06-21T15:00:00.000Z"
                    }
                }]},
                {"name": "C2", "_id": "C2", "bookings": [{
                    "bookerName": "C. Müller",
                    "time": {
                        "from": "2019-06-21T20:45:00.000Z",
                        "to": "2019-06-21T21:00:00.000Z"
                    }
                }]},
                {"name": "Salon", "_id": "Salon", "bookings": [{
                    "bookerName": "A. Villiger",
                    "time": {
                        "from": "2019-06-21T09:30:00.000Z",
                        "to": "2019-06-21T10:00:00.000Z"
                    }
                }]},
                {"name": "Loft", "_id": "Loft", "bookings": [{
                    "bookerName": "M. Huber",
                    "time": {
                        "from": "2019-07-21T11:15:00.000Z",
                        "to": "2019-07-21T11:45:00.000Z"
                    }
                }]}
            ]
        }
    }
}
