#!/usr/bin/env sh

# this script installs all requirements for running the screenhub client on a raspberry PI (or any other debian based system).
# make sure we are in the right directory fist:
if [[ $pwd != "/home/pi/hubscreen/packages/client" ]]; then
    echo "invalid working directory. Execute this script at: /home/pi/hubscreen/packages/client" && exit 1
fi

# make sure we're up to date:
apt get update

# First install the required python version and global python packages to run the hubscreen client:
# Install pip3:
sudo apt-get install python3-pip
# Install pipenv:
pip3 install --user pipenv

# pipenv will be installed to ~/.local/bin/pipenv, add directory to PATH:
echo "PATH=$PATH:~/.local/bin" >> ~/.bashrc && source ~/.bashrc

# Before any python dependencies can be installed, some system dependencies have to be added first:
sudo apt install libgraphicsmagick++1-dev libboost-python-dev -y

# Next activate the pipenv environement and install all dependencies (make sure you're at `~/hubscreen/packages/client`):
pipenv shell
pipenv install -v
