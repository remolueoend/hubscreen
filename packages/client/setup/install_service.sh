#!/usr/bin/env bash

# create symlink of service file in styemd unit directory:
ln -s /home/pi/hubscreen/packages/client/hubscreen_client.service /etc/systemd/system/hubscreen_client.service 

# initially start service
systemctl start hubscreen_client

# enable service to restart after stystem reboot:
systemctl enable hubscreen_client
