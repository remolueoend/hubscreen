#!/usr/bin/env bash

pushd /home/pi/hubscreen/

git fetch

# validate if an update is necessary. Crashes when local repo contains commits not pushed yet
# or when local and remote branches diverged:
UPSTREAM=${1:-'@{u}'}
LOCAL=$(git rev-parse @)
REMOTE=$(git rev-parse "$UPSTREAM")
BASE=$(git merge-base @ "$UPSTREAM")

if [ $LOCAL = $REMOTE ]; then
    echo "[INFO] Already up to date. Aborting update."
    exit 0
elif [ $REMOTE = $BASE ]; then
    echo "[ERROR] Local repo contains commits not pushed to remote yet. Cleanup and execute this command again. Aborting update." 1>&2
    exit 1
elif [ $LOCAL = $BASE ]; then
    echo "[INFO] Update available. Pulling and restarting service..."
    git pull
    systemctl restart hubscreen_client.service
    echo "[INFO] Service updated and restarted."
    exit 0
else
    echo "[ERROR] Local and remote master branches diverged. Cleanup and execute this command again. Aborting update." 1>&2
    exit 1
fi


