#!/usr/bin/env sh

# see: https://github.com/hzeller/flaschen-taschen/blob/master/server/README.md#rgb-matrix-panel-display
# how to build/setup the ft-server properly.
sudo ./ft-server --led-chain=10 --led-brightness=50 --led-slowdown-gpio=2 --led-no-hardware-pulse
