# Tools
This readme assumes you're using some kind of Linux, macOS should be very similar.

## Backup SD-card to *.img file
1. Connect the SD-card with your device, *but do not mount it*
2. use `sudo fdisk -l` to get the name of the card, e.g. `/dev/dev/mmcblk0`.
   The `p1/p2` or `1/2` suffix identifies the partition, but we want to backup the whole card. 
3. Use `sudo dd status=progress if=/dev/<card_id> of=</your/output/path.img>`, i.e: `sudo dd if=/dev/mmcblk0 of=/home/foo/bar.img`
4. The image has the same size as the SD-card (64GB) but we can shrink and compress it using the `pishring.sh` script in this directory:
   `./pshrink.sh [-v] -z -a /your/output/path.img /your/output/path.img.gzip`. See https://github.com/Drewsif/PiShrink for more details.
   
   
## Restore *.img file to SD-card
1. If the image to restore has extension `*.img.gz`, you nee `gzip` to decompress the image first.
2. Use `fdisk -l` to find the label of the SD-card device (*not partition!*), e.g: `/dev/mmcblk0`
3. Make sure the device is unmounted. Use `sudo mount | grep <device_name>` to check.
4. write the backup to the SD-card: `sudo dd if=<path to backup.img> of=/dev/<device-name>`. *Double-check* to make sure not to mess up the device name and accidentally overwrite another device on your computer!
5. If the image was shrunk using `pishrink` (see above), the initial startup will take longer than usual, while the image is automatically expanded to the SD-card size.
