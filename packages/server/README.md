# Hubscreen Flaschentaschen Server

## Overview

This package only contains a README, some helper scripts and the actual source code of the flaschentaschen server as a GIT submodule pointing to [hzeller/flaschentaschen server](https://github.com/hzeller/flaschen-taschen). After compiling and starting, the server will listen on UDP port 1337 for incoming bitmap frames and renders them on the screen. This interface is then used by the `client` package, which generates bitmap images and simply sends them to this server over a UDP connection.

## Setup on the Raspberry PI

The following documentation assumes you followed the [initial setup steps](../../README.md) first.

The code of the server is contained in this repo as a submodule under `packages/server/flaschentaschen`, that's why it is important to recursively clone this repo. Following commands are all executed in the directory `~/hubscreen/packages/server`.

First compile the flaschentaschen server:

```sh
./compile_server.sh
```

The server can be manually started using following command. But for normal operation, preferably manage the server via a systemd service, as described below.

```sh
sudo ./start_server.sh
```

Next step is to install and enable the server as a systemd service, to restart the server automatically after crashes and system reboots:

```sh
sudo ./install_service.sh
```

After executing the commands above, the flaschentaschen server should already be running and listening on port 1337. It also should automatically start after a system reboot.

To verify the correct setup of the flaschentaschen service, use `systemctl` and `journalctl` commands, such as `journalctl -u flaschentaschen.service` or `systemctl status flaschentaschen.service`.
