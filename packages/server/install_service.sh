#!/usr/bin/env bash

# create symlink of service file in styemd unit directory:
ln -s /home/pi/hubscreen/packages/server/flaschentaschen.service /etc/systemd/system/flaschentaschen.service 

# initially start service
systemctl start flaschentaschen

# enable service to restart after stystem reboot:
systemctl enable flaschentaschen
