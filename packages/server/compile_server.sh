#!/usr/bin/env bash

# compiles the flaschentaschen server.
# Executable will be located at: `~/hubscreen/packages/server/flaschentaschen/server/ft-server`

pushd ~/hubscreen/packages/server/flaschentaschen/server
# build the flaschentaschen server for the `rgb-matrix` backend,
# see: https://github.com/hzeller/flaschen-taschen/tree/master/server#rgb-matrix-panel-display
make FT_BACKEND=rgb-matrix
popd
